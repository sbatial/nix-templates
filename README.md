[![built with nix](https://img.shields.io/badge/-Built%20with%20nix-white?style=flat-square&logo=NixOS)](https://builtwithnix.org)
[![GitLab last commit](https://img.shields.io/gitlab/last-commit/sbatial%2Fnix-templates?style=flat-square&logo=gitlab&labelColor=white&color=%23B6E2A1)](https://gitlab.com/sbatial/nix-templates/-/commits/trunk/)
[![MIT License](https://img.shields.io/badge/-MIT%20License-white?style=flat-square)](https://mit-license.org/)

Some templates I gathered over time.

---

All environments should try to activate automatically if [direnv](https://direnv.net/) is installed.


Currently the following languages/environments are present:

[![Rust logo](https://img.shields.io/badge/-Rust-C45508?logo=rust&logoColor=black&style=flat-square)](https://www.rust-lang.org/)
[![mdBook logo](https://img.shields.io/badge/-mdBook-000000?logo=mdBook&logoColor=ffffff&style=flat-square)](https://rust-lang.github.io/mdBook/)
[![C logo](https://img.shields.io/badge/-C-A8B9CC?logo=C&logoColor=ffffff&style=flat-square)](https://www.open-std.org/jtc1/sc22/wg14/)
[![nix snowflake logo](https://img.shields.io/badge/-nix-5277C3?logo=NixOS&logoColor=000000&style=flat-square)](https://nixos.org/manual/nix/stable/language/index.html)

# Rust (using [crane](https://crane.dev/))

I'm using crane now when setting up new projects; mostly because of the incremental compilation.

## Usage

```sh
nix flake init -t gitlab:sbatial/nix-templates#rust-crane
```

## Tooling

- [treefmt](https://github.com/numtide/treefmt)
    * [rustfmt](https://github.com/rust-lang/rustfmt)
    * [alejandra](https://github.com/kamadorueda/alejandra) (nix)
- [rust-analyzer](https://github.com/rust-lang/rust-analyzer) (the environment has the relevant variables set so editors should pick it up correctly)

# Rust (using [naersk](https://github.com/nix-community/naersk))

## Usage

```sh
nix flake init -t gitlab:sbatial/nix-templates#rust-naersk
```

## Tooling

- [treefmt](https://github.com/numtide/treefmt)
    * [rustfmt](https://github.com/rust-lang/rustfmt)
    * [alejandra](https://github.com/kamadorueda/alejandra) (nix)
- [rust-analyzer](https://github.com/rust-lang/rust-analyzer) (the environment has the relevant variables set so editors should pick it up correctly)

# [mdBook](https://github.com/rust-lang/mdBook)

Has the `mdbook` installed and `mdbook init` already run to be able to just start using it.

## Usage

```sh
nix flake init -t gitlab:sbatial/nix-templates#mdbook
```

## Tooling

- mdbook

# C

A simple C environment with `clang` and `meson`.

## Usage

```sh
nix flake init -t gitlab:sbatial/nix-templates#c
```

## Tooling

- [meson](https://mesonbuild.com/)
- some scripts to compile, run and test the main file
    * `compile`: compiles main using gcc
    * `crun`: invokes compile and then _runs the resulting binary_
    * `runtest`: invokes compile and then runs the resulting binary _ten times_ in a row with a _second pause between each invokation_
    * `mrun`: compiles main _using meson_ and runs the resulting binary

