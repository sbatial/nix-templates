{
  description = "Collection of project templates.";

  outputs = _: {
    templates = {
      rust-crane = {
        path = ./rust-crane;
        description = "Basic Rust environment using crane, a (working) toolchain and treefmt.";
      };
      rust-naersk = {
        path = ./rust-naersk;
        description = "Basic Rust environment using naersk, a (working) toolchain and treefmt.";
      };
      mdbook = {
        path = ./mdbook;
        description = "Simple mdbook project.";
      };
      c = {
        path = ./c-devenv;
        description = "Basic C environment with meson, clangd, and some useful scripts";
      };
    };
  };
}
