{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
    systems.url = "github:nix-systems/default";
    devenv.url = "github:cachix/devenv";
  };

  outputs = {
    self,
    nixpkgs,
    devenv,
    systems,
    ...
  } @ inputs: let
    forEachSystem = nixpkgs.lib.genAttrs (import systems);
  in {
    devShells =
      forEachSystem
      (system: let
        pkgs = nixpkgs.legacyPackages.${system};
      in {
        default = devenv.lib.mkShell {
          inherit inputs pkgs;
          modules = [
            {
              packages = with pkgs; [pkg-config ninja meson clang-tools llvmPackages_15.clang-unwrapped gdb];

              env.DEFAULT_FILE = "main";

              pre-commit.hooks.clang-format.enable = true;

              scripts = {
                compile.exec = "${pkgs.gcc}/bin/gcc $DEFAULT_FILE.c -o $DEFAULT_FILE -O0 -g";
                crun.exec = "compile && ./$DEFAULT_FILE $argv";
                runtest.exec = "compile && for i in \$(seq 1 10); do ./$DEFAULT_FILE; sleep 1; done";
                mrun.exec = "${pkgs.meson}/bin/meson compile -C build && echo 'Start execution:' && ./build/$DEFAULT_FILE $argv";
              };
            }
          ];
        };
      });
  };
}
