{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    flake-utils,
    nixpkgs,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        pkgs = (import nixpkgs) {
          inherit system;
        };
      in {
        # For `nix fmt`
        # formatter = treefmtEval.config.build.wrapper;

        # For `nix build` & `nix run`:
        # packages.default = naersk'.buildPackage {
        #   src = ./.;
        # };

        # For `nix develop`:
        devShells.default = pkgs.mkShell {
          nativeBuildInputs = with pkgs; [mdbook];
        };
      }
    );
}
